BEGIN TRAN
ROLLBACK

SELECT * FROM [dbo].[tblAdminMaintenance]

UPDATE [dbo].[tblAdminMaintenance]
SET MaintenanceMode = 1


SELECT * FROM Localization.Globalization_Resources WHERE Resource_Name LIKE 'SSOMaintenanceNotice%'
SELECT * FROM Localization.Globalization_Resources WHERE Resource_Value LIKE '%SITE IN MAINTANENCE MODE%'

UPDATE Localization.Globalization_Resources
SET Resource_Value = ''
WHERE Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode'

UPDATE Localization.Globalization_Resources SET Resource_Value = N'We are improving your feedback experience.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name = 'en-GB'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Poprawiamy jakość twoich opinii.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name = 'pl-PL'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Stiamo migliorando la tua esperienza di feedback.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name = 'it-IT'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Geri bildirim deneyiminizi geliştiriyoruz.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name = 'tr-TR'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Мы улучшаем ваши отзывы.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name = 'ru-RU'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Wir verbessern Ihr Feedback-Erlebnis.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name IN ('de-DE', 'de-AT')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Estamos mejorando su experiencia de retroalimentación.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name IN ('es-MX', 'es-ES', 'es-AR', 'es-CL')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Nous améliorons votre expérience de rétroaction.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name IN ('fr-FR', 'fr-BE')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Estamos melhorando sua experiência de feedback.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name IN ('pt-BR', 'pt-PT')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'We verbeteren uw feedbackervaring.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name IN ('nl-NL', 'nl-BE')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Ми покращуємо ваш досвід відгуку.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name IN ('uk', 'uk-UA')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Poboljšamo vaše iskustvo povratnih informacija.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name = 'hr-HR'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Poboljšamo vaše iskustvo povratnih informacija.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name = 'sr-Latn-CS'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Izboljšujemo vaše izkušnje s povratnimi informacijami.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name = 'sl-SI'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Fejlesztjük az Ön visszajelzési tapasztalatait.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name = 'hu-HU'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Îți îmbunătățim experiența de feedback. Vă rugăm să încercați din nou mai târziu.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details' AND Culture_Name = 'ro-RO'
							
							
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Please try again later.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name = 'en-GB'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Spróbuj ponownie później.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name = 'pl-PL'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Per favore riprova più tardi.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name = 'it-IT'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Lütfen daha sonra tekrar deneyiniz.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name = 'tr-TR'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Пожалуйста, попробуйте позже.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name = 'ru-RU'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Bitte versuchen Sie es später noch einmal.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name IN ('de-DE', 'de-AT')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Por favor, inténtelo de nuevo más tarde.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name IN ('es-MX', 'es-ES', 'es-AR', 'es-CL')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Veuillez réessayer plus tard.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name IN ('fr-FR', 'fr-BE')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Por favor, tente novamente mais tarde.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name IN ('pt-BR', 'pt-PT')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Probeer het later opnieuw.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name IN ('nl-NL', 'nl-BE')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Будь-ласка спробуйте пізніше.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name IN ('uk', 'uk-UA')
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Molimo pokušajte ponovo kasnije.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name = 'hr-HR'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Pokušajte ponovo kasnije.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name = 'sr-Latn-CS'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Prosim poskusite kasneje.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name = 'sl-SI'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Kérlek, próbáld újra később.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name = 'hu-HU'
UPDATE Localization.Globalization_Resources SET Resource_Value = N'Vă rugăm să încercați din nou mai târziu.' WHERE  Resource_Name = 'SSOMaintenanceNotice_Retry' AND Culture_Name = 'ro-RO'

INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'We are improving your feedback experience.', 'en-GB', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Poprawiamy jakość twoich opinii.', 'pl-PL', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Stiamo migliorando la tua esperienza di feedback.', 'it-IT', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Geri bildirim deneyiminizi geliştiriyoruz.', 'tr-TR', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Мы улучшаем ваши отзывы.', 'ru-RU', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Wir verbessern Ihr Feedback-Erlebnis.', 'de-DE', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Wir verbessern Ihr Feedback-Erlebnis.', 'de-AT', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Estamos mejorando su experiencia de retroalimentación.', 'es-MX', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Estamos mejorando su experiencia de retroalimentación.', 'es-ES', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Estamos mejorando su experiencia de retroalimentación.', 'es-AR', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Estamos mejorando su experiencia de retroalimentación.', 'es-CL', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Nous améliorons votre expérience de rétroaction.', 'fr-FR', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Nous améliorons votre expérience de rétroaction.', 'fr-BE', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Estamos melhorando sua experiência de feedback.', 'pt-BR', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Estamos melhorando sua experiência de feedback.', 'pt-PT', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'We verbeteren uw feedbackervaring.', 'nl-NL', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'We verbeteren uw feedbackervaring.', 'nl-BE', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Ми покращуємо ваш досвід відгуку.', 'uk', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Ми покращуємо ваш досвід відгуку.', 'uk-UA', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Poboljšamo vaše iskustvo povratnih informacija.', 'hr-HR', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Poboljšamo vaše iskustvo povratnih informacija.', 'sr-Latn-CS', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Izboljšujemo vaše izkušnje s povratnimi informacijami.', 'sl-SI', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Fejlesztjük az Ön visszajelzési tapasztalatait.', 'hu-HU', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Site_In_Maintanence_Mode_Details', N'Îți îmbunătățim experiența de feedback. Vă rugăm să încercați din nou mai târziu.',  'ro-RO', 0)
				
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Please try again later.',  'en-GB', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Spróbuj ponownie później.',  'pl-PL', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Per favore riprova più tardi.',  'it-IT', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Lütfen daha sonra tekrar deneyiniz.',  'tr-TR', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Пожалуйста, попробуйте позже.',  'ru-RU', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Bitte versuchen Sie es später noch einmal.',  'de-DE', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Bitte versuchen Sie es später noch einmal.',  'de-AT', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Por favor, inténtelo de nuevo más tarde.',  'es-MX', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Por favor, inténtelo de nuevo más tarde.',  'es-ES', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Por favor, inténtelo de nuevo más tarde.',  'es-AR', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Por favor, inténtelo de nuevo más tarde.',  'es-CL', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Veuillez réessayer plus tard.',  'fr-FR', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Veuillez réessayer plus tard.',  'fr-BE', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Por favor, tente novamente mais tarde.',  'pt-BR', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Por favor, tente novamente mais tarde.',  'pt-PT', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Probeer het later opnieuw.',  'nl-NL', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Probeer het later opnieuw.',  'nl-BE', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Будь-ласка спробуйте пізніше.',  'uk', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Будь-ласка спробуйте пізніше.',  'uk-UA', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Molimo pokušajte ponovo kasnije.',  'hr-HR', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Pokušajte ponovo kasnije.',  'sr-Latn-CS', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Prosim poskusite kasneje.',  'sl-SI', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Kérlek, próbáld újra később.',  'hu-HU', 0)
INSERT INTO Localization.Globalization_Resources (Resource_Object, Resource_Name, Resource_Value, Culture_Name, AdminOnly) VALUES ('WebHostStrings', 'SSOMaintenanceNotice_Retry', N'Vă rugăm să încercați din nou mai târziu.',  'ro-RO', 0)
				